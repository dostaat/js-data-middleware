const name = 'cookie';       

function set(key, value, [ttl]) {
	let expire = "expires= "+ (ttl ? ttl : new Date(new Date().setFullYear(new Date().getFullYear()+1)))
   document.cookie(key+"="+value+";"+expires)
}

function get(key) {
  var name = key + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function delete(key) {
	document.cookie = key + "=;expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
}