const name = 'IndexedDB';
var db;

function supportCheck(){
	window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
	return window.IndexedDB;
}

function openOrCreate(){
	if(this.supportCheck()){
		var request = indexedDB.open("miuCacheDB");
		request.onerror = function(event) {
		  	console.log("I could not connect to IndexedDB.");
		};
		request.onsuccess = function(event) {
		  	db = event.target.result;
		};
	}
}

function set(table, key, value){
	if (!db){
		this.openOrCreate();
	}
	var transaction = db.transaction([table], "readwrite");
	transaction.oncomplete = function(event) {
		console.log("All done!");
	};
	transaction.onerror = function(event) {
			console.log("Some error occurred during the transaction: " + transaction.error );
	};
	var objectStore = transaction.objectStore(type);
	var request = objectStore.add(value, key);
	request.onsuccess = function(event) {
		// event.target.result === customer.ssn;
	};
}

function get(table, key){
	if (db){	
		var transaction = db.transaction([table], "readonly");
		transaction.oncomplete = function(event) {
			console.log("All done!");
		};
		transaction.onerror = function(event) {
				console.log("Some error occurred during the transaction: " + transaction.error );
		};
		var objectStore = transaction.objectStore(type);
		var request = objectStore.get(key);
		request.onsuccess = function(event) {
			// event.target.result === customer.ssn;
			return request.result;
		};
	} else {
		return null;
	}
}

function delete(table, key){
	var transaction = db.transaction([table], "readwrite");
	transaction.oncomplete = function(event) {
		console.log("All done!");
	};
	transaction.onerror = function(event) {
			console.log("Some error occurred during the transaction: " + transaction.error );
	};
	var objectStore = transaction.objectStore(type);
	var request = objectStore.delete(key);
	request.onsuccess = function(event) {
		// event.target.result === customer.ssn;
	};
}

function clear(table){
	var transaction = db.transaction([table], "readwrite");
	transaction.oncomplete = function(event) {
		console.log("All done!");
	};
	transaction.onerror = function(event) {
			console.log("Some error occurred during the transaction: " + transaction.error );
	};
	var objectStore = transaction.objectStore(type);
	var request = objectStore.clear();
	request.onsuccess = function(event) {

	};
}