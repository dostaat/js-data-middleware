const name = 'localStorage';

function set(key, value){
	window.localStorage.setItem(key, JSON.stringify(value));
}

function get(key){
	return window.localStorage.getItem(key);
}

function delete(key){
	window.localStorage.removeItem(key);
}

function clear(){
	window.localStorage.clear();
}